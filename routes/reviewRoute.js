const express = require("express");
const router = express.Router();

// Import Controller
const ReviewController = require("../controllers/reviewController");

//Import Validator
const ReviewValidator = require("../middlewares/validators/reviewValidator")

router.post("/",ReviewValidator.create,ReviewController.create); // Create data
router.get("/",ReviewValidator.getAll,ReviewController.getAll); // Get all data with pages
router.get("/:id", ReviewValidator.getOne,ReviewController.getOne); // Get one Data
router.put("/:id", ReviewController.update); // Update data
router.delete("/:id", ReviewController.delete); // delete data


module.exports = router;
