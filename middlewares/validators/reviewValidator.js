const { review } = require("../../models");
const mongoose = require("mongoose");
const path = require("path");
const validator = require("validator");
const crypto = require("crypto");

exports.create = async (req, res, next) => {
    let Errors = [];
    try {

        if (validator.isAlphanumeric(req.body.rating)) { // Check only contain number
            Errors.push("Rating isn't a number")
        } else if (!validator.isInt(req.body.rating)) { //check only contain integer
            Errors.push("Rating isn't an Integer")
        };

        if (req.body.rating > 10 || req.body.rating < 1) { //check rating must around 1 to 10
            Errors.push("Rating is out of range")
        }

        if (Errors.length > 0) {
            return res.status(400).json({
                message: Errors.join(", "),
            })
        }
        console.log(Errors)
        next()
    } catch (error) {
        return res.status(500).json({
        message: "Internal Server Error",
            error: error.message,
        })
    }

}

exports.getAll = async (req,res,next)=>{
    let Errors = [];
    try {
        if(!validator.isInt(req.query.page)){
            Errors.push("Page must be a number")
        }
        
        if(!validator.isInt(req.query.perpage)){
            Errors.push("limit document perpage must be a number")
        }

        if(Errors.length>0){
            return res.status(401).json({
                message: Errors.join(', ')
            })
        }
        next()
    } catch (error) {
        return res.status(500).json({
        message: "Internal Server Error",
            error: error.message,
        })
    }
}

exports.getOne = (req, res, next) => {
    try {
      // Check parameter is valid or not
      if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        return res.status(400).json({
          message:
            "Parameter is not valid and must be 24 character & hexadecimal",
        });
      }
  
      next();
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  };