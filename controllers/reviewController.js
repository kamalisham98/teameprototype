const { review, movie, user } = require("../models") // import models

class ReviewController {
  async getAll(req, res) {
    try {

      // Pagination option
      const { page, perpage } = req.query;
      let options = {
         page: parseInt(page, 10), // jump to witch page
          limit: parseInt(perpage, 10) // determine data each page
         }
      let data = await review.paginate({}, options);
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }

  async getAllReview(req,res){
    try {

      // Pagination option
      const { page, perpage } = req.query;
      let options = {
         page: req.query.page, // jump to witch page
          limit: req.query.limit, // determine data each page
         }
      let data = await review.paginate({user:req.query.userid}, options);
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }

  async getOne(req, res) {
    try {
      let data = await review.findOne({ _id: req.params.id });
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }

  async create(req, res) {
    try {
      let createData = await review.create(
        req.body
        // Declare one by one
        // {
        // review: req.body.review,
        // rating: req.body.rating,
        // user: null,
        // Movie_id: null,
        // }
      );

      let data = await review.findOne({ _id: createData._id })
      return res.status(201).json({
        message: " Success",
        data: data,
      })
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: "Internal Sever Error",
        error: error,
      })

    }
  }

  async update(req, res) {
    try {
      // Update data
      let data = await review.findOneAndUpdate(
        {
          _id: req.params.id,
        },
        req.body, // This is all of req.body
        {
          new: true,
        }
      );
      // new is to return the updated transaksi data
      // If no new, it will return the old data before updated

      // If success
      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }

  async delete(req, res) {
    try {
      // delete data
      await review.delete({ _id: req.params.id });

      return res.status(200).json({
        message: "Success",
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }
}


module.exports = new ReviewController;