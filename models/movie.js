const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const MovieSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    year: {
      type: String,
      required: true,
    },
    poster: {
      type: String,
      default: null,
      required: false,
      get: getImage,
    },
    trailer: {
      type: String,
      required: true,
    },
    synopsis: {
      type: String,
      required: true,
    },
    genre: [
      {
        type: String,
        required: true,
      },
    ],
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true },
  }
);

function getImage(poster) {
  if (!poster) {
    return null;
  }

  return `/images/${poster}`;
}

MovieSchema.plugin(mongooseDelete, { overideMethods: "all" });

module.exports = mongoose.model("movie", MovieSchema);
