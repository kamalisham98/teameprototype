const mongoose = require("mongoose"); // import mongoose
const mongooseDelete = require("mongoose-delete"); //import mongoose-delete
const mongoosePaginate = require("mongoose-paginate-v2") //import mongoose paginate

// Create mongoose Collection
const ReviewSchema = new mongoose.Schema({
    review: {
        type: String,
        required: true,
    },
    rating: {
        type: Number,
        required: true,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: false,
    },
    Movie:{
        type: mongoose.Schema.Types.ObjectId,
        ref:"Movie",
        required: false,
    }

}, {
    // Enable timestamps
    timestamps: {
        createdAt: "createAt",
        updateAt: "updateAt",
    },
    toJSON: { getters: true } // Enable getter
}
)


// Enable soft Delete
ReviewSchema.plugin(mongooseDelete,{overrideMethodes:"all"});

//Enable pagination
ReviewSchema.plugin(mongoosePaginate);

module.exports = mongoose.model("review",ReviewSchema, "review"); // export review's model